## Preparation

This project was build and tested with npm 6.10.3 and node 12.9.0

## Getting started

```bash
# install dependency
npm install

# develop
npm run dev
```

This will automatically open http://localhost:8001.

## Build

```bash
# build for test environment
npm run build:sit

# build for production environment
npm run build
```
