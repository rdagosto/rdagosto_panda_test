module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  API: '"//swapi.co/api/"',
  MOCK_SERVER: false
}
