module.exports = {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  API: '"https://swapi.co/api/"',
  MOCK_SERVER: false
}
