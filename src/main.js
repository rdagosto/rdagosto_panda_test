import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

/* eslint-disable */
import '@/styles/index.scss' // global css

import App from '@/App'
import store from '@/store'
import router from '@/router'
import VueResource from 'vue-resource'
import VueCookies from 'vue-cookies'
import VueMoment from 'vue-moment'

import '@/icons' // icon

import * as filters from '@/filters' // global filters

import VueResourceMock from 'vue-resource-mock'
import MockData from '@/mockdata.js'

Vue.use(VueMoment)
Vue.use(VueCookies)
Vue.use(VueResource)

if (process.env.MOCK_SERVER) {
  Vue.use(VueResourceMock, MockData, /* { silent: true/false } */ )
}
locale.use(lang)
Vue.use(ElementUI)

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
