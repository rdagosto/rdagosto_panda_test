/* eslint-disable */
import { Message } from 'element-ui'
export default class util {
    static getBaseUrl(){
        return process.env.API
    }
    static getCaptchaCode(){
        return process.env.CAPTCHA_CODE
    }

    static getFile(response) {
        var result = document.createElement('a');
        var contentDisposition = response.headers.get('Content-Disposition') || '';
        var filename = contentDisposition.split('filename=')[1];
        filename = filename.replace(/"/g,"")

        return response.blob()
          .then(function(data) {
            result.href = window.URL.createObjectURL(data);
            result.target = '_self';
            result.download = filename;

            return result;
          })
  }

  static showMessage(msg, tipo, close = false) {
      if(tipo){
          Message({
              showClose: close,
              message: msg,
              type: tipo,
              center: true
          });
      } else {
          Message({
              showClose: close,
              message: msg,
              center: true
          });
      }
  }

  static geracor(quantidade = 1){
    var retorno = []
    for (var j = 0; j < quantidade; j++ ) {
      var hexadecimais = '0123456789ABCDEF';
      var cor = '#';

      // Pega um número aleatório no array acima
      for (var i = 0; i < 6; i++ ) {
      //E concatena à variável cor
          cor += hexadecimais[Math.floor(Math.random() * 16)];
          if(retorno.indexOf(cor) != -1){
            j--
          }
      }
      retorno.push(cor);
    }
    return retorno
  }
}
