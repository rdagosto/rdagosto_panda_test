/* eslint-disable */
export default class error {
  static toHtml(errors) {
    if (typeof errors === 'string') {
      return errors
    } else {
      let msg = ''
      Object.keys(errors).forEach(obj => {
        msg = obj + ': ' + errors[obj]
      })
      return msg
    }
  }
}
