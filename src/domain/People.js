import DomainParent from '@/domain/parent/DomainParent.js'

/* eslint-disable */
export default class People extends DomainParent {
  constructor( data ) {
    super(data)
    this.name = data.name
    this.height = data.height
    this.mass = data.mass
    this.hair_color = data.hair_color
    this.skin_color = data.skin_color
    this.eye_color = data.eye_color
    this.birth_year = data.birth_year
    this.gender = data.gender
    this.homeworld = data.homeworld
    this.films = data.films
    this.species = data.species
    this.vehicles = data.vehicles
    this.created = data.created
    this.edited = data.edited
  }
}
