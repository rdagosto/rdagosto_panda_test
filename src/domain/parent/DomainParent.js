/* eslint-disable */
export default class DomainParent {
  constructor(data) {
    if (data != null) {
      this.id = data.id
    } else {
      this.id = 0
    }
  }

  hasId() {
    return ((this.id != null) && (this.id != '') && (this.id > 0))
  }
}
