import InfoService from '@/service/InfoService.js'

/* eslint-disable */
export default class PeopleService extends InfoService {

  constructor(ref) {
    super(ref)
  }

  get(id) {
    let url = `${super.baseUrl}people/${id}`
    return super.customGet(url)
  }

  list(page) {
    let url = `${super.baseUrl}people`
    return super.customGet(url, {page: page})
  }

  add(dto) {
    let url = `${super.baseUrl}people`
    return super.customPost(url, dto)
  }

  update(id, dto) {
    let url = `${super.baseUrl}people/${id}`
    return super.customPut(url, dto)
  }

  delete(id) {
    let url = `${super.baseUrl}people/${id}`
    return super.customDelete(url)
  }
}
