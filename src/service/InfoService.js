import Vue from 'vue'
import router from '@/router'
import error from '@/utils/error.js'

/* eslint-disable */
export default class InfoService {

  constructor(ref = null) {
    this._http = Vue.http
    this._ref = ref
  }

  get baseUrl() {
    return process.env.API
  }

  errorHandler(err) {
    if (err.status === 401) {
      router.push({ path: '/login' })
    } else {
      if (this._ref) {
        this._ref.$message.error(error.toHtml(err.data.errors))
      }
      return false
    }
  }

  customGet(url, params = [], contentType = 'application/json') {
    return this._http.get(url, { params: params }, { headers: { 'Content-Type': contentType }})
      .then(
        res => {
          if (res.ok) {
            return res
          } else {
            return false
          }
        },
        err => this.errorHandler(err)
      )
  }

  customGetDownload(url, params=[], contentType='application/json') {
    return this._http.get(url, {params:params}, {headers:{'Content-Type':contentType}, responseType: 'blob'})
      .then(
        res => {
          if (res.ok) {
            return res
          } else {
            return false
          }
        },
        err => this.errorHandler(err)
      )
  }

  customPost(url, dto=[], contentType='application/json', ) {
    return this._http.post(url, dto, {headers:{'Content-Type':contentType}})
      .then(
        res => {
          if (res.ok) {
            return res
          } else {
            return false
          }
        },
        err => this.errorHandler(err)
      )
  }

  customPut(url, dto=[], contentType='application/json') {
    return this._http.put(url, dto, {headers:{'Content-Type':contentType}})
      .then(
        res => {
          if (res.ok) {
            return res
          } else {
            return false
          }
        },
        err => this.errorHandler(err)
      )
  }

  customPatch(url, dto=[], contentType='application/json') {
    return this._http.patch(url, dto, {headers:{'Content-Type':contentType}})
      .then(
        res => {
          if (res.ok) {
            return res
          } else {
            return false
          }
        },
        err => this.errorHandler(err)
      )
  }

  customDelete(url, dto=[], contentType='application/json') {
    Vue.http.headers.common['Content-Type'] = contentType
    return this._http.delete(url, dto, {headers:{'Content-Type':contentType}})
      .then(
        res => {
          if (res.ok) {
            return res
          } else {
            return false
          }
        },
        err => this.errorHandler(err)
      )
  }
}
