/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import { auth } from '@/main'
import Layout from '@/views/layout/Layout'

Vue.use(Router)

export default new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '',
      name: 'home',
      component: Layout,
      redirect: 'dashboard',
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          component: () => import('@/views/dashboard/index'),
          meta: { title: 'Dashboard', icon: 'dashboard', show: true }
        }
      ]
    },
  ]
})
